import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mobology/src/core/network/network_info.dart';
import 'package:mobology/src/data/repositories/repository.dart';
import 'package:mobology/src/data/resources/local_data_source.dart';
import 'package:mobology/src/data/resources/remote_data_source.dart';
import 'package:mobology/src/domain/repositories/repository.dart';
import 'package:mobology/src/domain/usecases/get_trending_devices.dart';
import 'package:mobology/src/presentation/bloc/bloc.dart';

final sl = GetIt.instance;
Future<void> init() async {
  sl.registerFactory(() => HomeBloc(getDevices: sl()));

  // usecases
  sl.registerLazySingleton(() => GetTrendingDevices(sl()));

  sl.registerLazySingleton<Repository>(
    () => MobologyRepository(
      localDataSource: sl(),
      remoteDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  sl.registerLazySingleton<LocalDataSource>(() => LocalDataSourceI());
  sl.registerLazySingleton<RemoteDataSource>(
      () => RemoteDataSourceI(client: sl()));
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoI(sl()));

  // Externals
  sl.registerLazySingleton(() => Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}
