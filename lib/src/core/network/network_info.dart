import 'package:data_connection_checker/data_connection_checker.dart';

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class NetworkInfoI extends NetworkInfo {
  final DataConnectionChecker _checker;
  NetworkInfoI(this._checker);

  @override
  Future<bool> get isConnected => _checker.hasConnection;
}
