import 'package:dartz/dartz.dart';
import 'package:mobology/src/core/error/failure.dart';

class InputConverter {
  Either<Failure, int> stringToUnsignedInteger(String str) {}
}

class InvalidInputFailure extends Failure {
  @override
  List<Object> get props => [];
}
