import 'package:flutter/material.dart';
import 'package:mobology/src/presentation/screens/home.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(0xff363636),
        primaryColorDark: Color(0xff282828),
        primaryColorLight: Color(0xff969696),
        accentColor: Color(0xff045757),
        canvasColor: Color(0xff044343),
        cardColor: Color(0xffe4e4e4),
      ),
      onGenerateRoute: routes,
      home: HomeScreen(),
    );
  }

  Route routes(RouteSettings settings) {
    final String name = settings.name;
    if (name == '/') {
      return MaterialPageRoute(builder: (context) => HomeScreen());
    }
  }
}
