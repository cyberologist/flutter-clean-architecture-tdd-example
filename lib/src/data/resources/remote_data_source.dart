import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:mobology/src/core/error/exception.dart';
import 'package:mobology/src/data/models/trending_devices_model.dart';

abstract class RemoteDataSource {
  // throws serverException for error codes
  Future<TrendingDevicesModel> getTrendingDevices();
}

const String BASE_URL = 'https://mobology.herokuapp.com';

class RemoteDataSourceI extends RemoteDataSource {
  final Client client;

  RemoteDataSourceI({@required this.client});

  @override
  Future<TrendingDevicesModel> getTrendingDevices() async {
    final Response response = await client.get(
        '$BASE_URL/api/mobiles/trending?api_key=ILoveHadia',
        headers: {'Content-type': 'application/json'});
    if (response.statusCode == 200) {
      return TrendingDevicesModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
