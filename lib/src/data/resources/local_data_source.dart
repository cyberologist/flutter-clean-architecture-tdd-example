import 'dart:io';

import 'package:mobology/src/core/error/exception.dart';
import 'package:mobology/src/data/models/trending_devices_model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

abstract class LocalDataSource {
  Future<TrendingDevicesModel> getLastTrendingDevices();
  Future<void> cacheTrendingDevices(TrendingDevicesModel devicesToCaches);
  void init();
}

const String TABLE_NAME = 'trending_devices';

class LocalDataSourceI extends LocalDataSource {
  Database db;
  LocalDataSourceI() {
    init();
  }
  @override
  Future<void> cacheTrendingDevices(TrendingDevicesModel devicesToCaches) {
    db.insert(
      'trending_devices',
      devicesToCaches.toDbModel(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  @override
  Future<TrendingDevicesModel> getLastTrendingDevices() async {
    final maps = await db.query(
      TABLE_NAME,
      columns: null, // ["title"]
      // where: "id = ? ",
      // whereArgs: [id],
    );

    if (maps.length > 0) {
      return TrendingDevicesModel.fromDb(maps.first);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> init() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentDirectory.path, "items.db");
    // open or create
    db = await openDatabase(path, version: 1,
        // called only when creating for the first time
        onCreate: (Database newDb, int version) {
      newDb.execute("""
            CREATE TABLE IF NOT EXISTS $TABLE_NAME
            (
            id INTEGER PRIMARY KEY , 
            last_devices_list TEXT 
            )
          """);
    });
  }
}
