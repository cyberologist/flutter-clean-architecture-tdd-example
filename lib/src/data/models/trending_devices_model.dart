import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:mobology/src/data/models/trending_device_model.dart';
import 'package:mobology/src/domain/entities/trending_device_entity.dart';
import 'package:mobology/src/domain/entities/trending_devices_entity.dart';

class TrendingDevicesModel extends TrendingDevicesEntity {
  final bool success;
  final String message;
  final int code;
  final List<TrendingDeviceModel> devices;
  TrendingDevicesModel({
    @required this.success,
    @required this.message,
    @required this.code,
    @required this.devices,
  }) : super(devices: devices.cast<TrendingDeviceEntity>());

  factory TrendingDevicesModel.fromJson(Map<String, dynamic> jsonMap) {
    return TrendingDevicesModel(
      success: jsonMap['success'],
      message: jsonMap['message'],
      code: jsonMap['code'],
      devices: jsonMap['data']
          .map(
            (deviceMap) => TrendingDeviceModel.fromJson(deviceMap),
          )
          .toList()
          .cast<TrendingDeviceModel>(),
    );
  }

  factory TrendingDevicesModel.fromDb(Map<String, dynamic> response) {
    Map<String, dynamic> map = json.decode(response['last_devices_list']);
    return TrendingDevicesModel.fromJson(map);
  }

  Map<String, dynamic> toJson() => {
        'success': success,
        'message': message,
        'code': code,
        'devices': devices.map((deviceModel) => deviceModel.toString())
      };

  @override
  String toString() => json.encode(toJson().toString());

  Map<String, dynamic> toDbModel() =>
      {'id': 1, 'last_devices_list': this.toString()};

  @override
  List<Object> get props => [success, message, code, devices];
}
