import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:mobology/src/domain/entities/trending_device_entity.dart';

class TrendingDeviceModel extends TrendingDeviceEntity {
  TrendingDeviceModel(
      {@required id,
      @required name,
      @required company,
      @required imageUrl,
      deviceType})
      : super(id: id, name: name, company: company, imageUrl: imageUrl);

  factory TrendingDeviceModel.fromJson(Map<String, dynamic> json) {
    return TrendingDeviceModel(
        id: json['id'],
        name: json['name'],
        company: json['company'],
        imageUrl: json['imageUrl'],
        deviceType: json['deviceType']);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "company": company,
      "imageUrl": imageUrl,
      "deviceType": deviceType
    };
  }

  String toString() {
    return json.encode(toJson());
  }
}
