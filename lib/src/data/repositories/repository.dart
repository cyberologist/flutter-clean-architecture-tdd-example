import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobology/src/core/error/exception.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/core/network/network_info.dart';
import 'package:mobology/src/data/resources/local_data_source.dart';
import 'package:mobology/src/data/resources/remote_data_source.dart';
import 'package:mobology/src/domain/entities/trending_devices_entity.dart';
import 'package:mobology/src/domain/repositories/repository.dart';

class MobologyRepository extends Repository {
  final RemoteDataSource remoteDataSource;
  final LocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  MobologyRepository({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, TrendingDevicesEntity>> fetchTrendingDevices() async {
    bool isConnetcted = await networkInfo.isConnected;
    if (isConnetcted) {
      try {
        final trendingDevices = (await remoteDataSource.getTrendingDevices());
        localDataSource.cacheTrendingDevices(trendingDevices);
        return Right(trendingDevices as TrendingDevicesEntity);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final trendingDevices = await localDataSource.getLastTrendingDevices();
        return Right(trendingDevices);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
