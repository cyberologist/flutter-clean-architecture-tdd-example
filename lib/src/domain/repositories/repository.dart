import 'package:dartz/dartz.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/domain/entities/trending_devices_entity.dart';

abstract class Repository {
  Future<Either<Failure, TrendingDevicesEntity>> fetchTrendingDevices();
}
