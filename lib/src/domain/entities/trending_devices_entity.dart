import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobology/src/domain/entities/trending_device_entity.dart';

class TrendingDevicesEntity extends Equatable {
  final List<TrendingDeviceEntity> devices;
  TrendingDevicesEntity({@required this.devices});

  @override
  List<Object> get props => devices;
}
