import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class TrendingDeviceEntity extends Equatable {
  final int id;
  final String name;
  final String company;
  final String deviceType;
  final String imageUrl;
  TrendingDeviceEntity(
      {@required this.id,
      @required this.name,
      @required this.company,
      @required this.imageUrl,
      this.deviceType});

  @override
  List<Object> get props => [id, name];
}
