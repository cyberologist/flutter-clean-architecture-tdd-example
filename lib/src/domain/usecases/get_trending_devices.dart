import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/core/usecase.dart';
import 'package:mobology/src/domain/entities/trending_devices_entity.dart';
import 'package:mobology/src/domain/repositories/repository.dart';

class GetTrendingDevices extends Usecase<TrendingDevicesEntity, NoParams> {
  final Repository repository;
  GetTrendingDevices(this.repository);

  Future<Either<Failure, TrendingDevicesEntity>> call(NoParams params) async {
    return repository.fetchTrendingDevices();
  }

  @override
  List<Object> get props => [];
}

class Params extends Equatable {
  @override
  List<Object> get props => [];
}
