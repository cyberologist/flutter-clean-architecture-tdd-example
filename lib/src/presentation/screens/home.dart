import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobology/src/presentation/bloc/bloc.dart';

import '../../../injection_container.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeBloc bloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = sl<HomeBloc>();
    bloc.add(GetTrendingDevicesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider.value(
        value: bloc,
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            print('got $state');
            if (state is Empty) {
              return Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Theme.of(context).primaryColor,
                      Theme.of(context).accentColor,
                    ],
                  ),
                ),
              );
            } else if (state is Loading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (state is Loaded) {
              return ListView.builder(
                itemCount: state.devices.length,
                itemBuilder: (context, index) {
                  return Text(state.devices[index].name);
                },
              );
            } else if (state is Error) {
              return Text(state.message);
            } else {
              return Container(
                child: Text("unexpectedError"),
              );
            }
          },
        ),
      ),
    );
  }
}
