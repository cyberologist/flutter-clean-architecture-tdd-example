import 'package:equatable/equatable.dart';
import 'package:mobology/src/domain/entities/trending_device_entity.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class Empty extends HomeState {
  @override
  List<Object> get props => [];
}

class Loading extends HomeState {
  @override
  List<Object> get props => [];
}

class Loaded extends HomeState {
  final List<TrendingDeviceEntity> devices;
  Loaded(this.devices);
  @override
  List<Object> get props => devices;
}

class Error extends HomeState {
  final String message;
  Error(this.message);
  @override
  List<Object> get props => [message];
}
