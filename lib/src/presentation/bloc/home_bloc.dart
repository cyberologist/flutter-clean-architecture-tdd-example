import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/core/usecase.dart';
import 'package:mobology/src/domain/usecases/get_trending_devices.dart';
import './bloc.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final GetTrendingDevices getDevices;

  HomeBloc({@required GetTrendingDevices getDevices})
      : assert(getDevices != null),
        this.getDevices = getDevices;

  @override
  HomeState get initialState => Empty();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is GetTrendingDevicesEvent) {
      yield Loading();
      final response = await getDevices(NoParams());
      yield response.fold(
        (failure) => Error(failure is CacheFailure
            ? CACHE_FAILURE_MESSAGE
            : SERVER_FAILURE_MESSAGE),
        (devices) => Loaded(devices.devices),
      );
    }
  }
}
