import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class GetTrendingDevicesEvent extends HomeEvent {
  static int id = 0;
  final int instanceId;
  GetTrendingDevicesEvent() : instanceId = id;
  @override
  List<Object> get props => [instanceId];
}
