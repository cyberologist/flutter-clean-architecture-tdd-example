import 'package:flutter/material.dart';
import 'src/app.dart';
import 'injection_container.dart' as DI;

void main() async {
  await DI.init();
  runApp(App());
}
