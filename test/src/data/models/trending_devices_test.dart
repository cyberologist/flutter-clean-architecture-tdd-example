import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/data/models/trending_device_model.dart';
import 'package:mobology/src/data/models/trending_devices_model.dart';

import '../../fixtures/fixtures_reader.dart';

void main() {
  group('fromJson', () {
    test(
        'getting the standard 200 ok response and return valid trinding devices model',
        () async {
      // same as the fixture
      final expectedModel = TrendingDevicesModel(
        success: true,
        code: 200,
        message: "OK",
        devices: [
          TrendingDeviceModel(
            id: 1,
            name: "samsung a40",
            company: "samsung",
            imageUrl: "www.imageUrl.com",
            deviceType: "tablet",
          ),
        ],
      );

      String jsonResponse = Fixture('trending_devices.json');
      final trendingDevices =
          TrendingDevicesModel.fromJson(json.decode(jsonResponse));

      expect(expectedModel, trendingDevices);
    });
  });
}
