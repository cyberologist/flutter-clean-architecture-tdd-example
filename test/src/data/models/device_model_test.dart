import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/data/models/trending_device_model.dart';

import '../../fixtures/fixtures_reader.dart';

void main() {
  // from the fixtures

  group('from json', () {
    final expectedDevice = new TrendingDeviceModel(
      id: 1,
      name: "samsung a40",
      company: "samsung",
      imageUrl: 'www.imageUrl.com',
      deviceType: 'tablet',
    );

    test('return a valid device model from the standard 200 response',
        () async {
      String jsonResponse = Fixture('trending_device.json');
      Map<String, dynamic> data = json.decode(jsonResponse);

      final device = TrendingDeviceModel.fromJson(data);
      expect(device, expectedDevice);
    });
  });
}
