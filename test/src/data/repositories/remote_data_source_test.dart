import 'dart:convert';

// import 'package:flutter/cupertino.dart' hide TypeMatcher;
import 'package:http/http.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/core/error/exception.dart';
import 'package:mobology/src/data/models/trending_devices_model.dart';
import 'package:mobology/src/data/resources/remote_data_source.dart';
import 'package:mockito/mockito.dart';
import 'package:matcher/matcher.dart';

import '../../fixtures/fixtures_reader.dart';

class MockHttpClient extends Mock implements Client {}

void main() {
  RemoteDataSourceI remoteDataSource;
  MockHttpClient httpClient;

  setUp(() {
    httpClient = MockHttpClient();
    remoteDataSource = RemoteDataSourceI(client: httpClient);
  });

  test('should performe GET request with the appropreit headers', () {
    when(httpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => Response(Fixture('trending_devices.json'), 200));

    remoteDataSource.getTrendingDevices();

    verify(httpClient.get('$BASE_URL/api/trending',
        headers: {'Content-type': 'application/json'}));
  });

  test('should return trendingDeviceModel when response code is 200 ',
      () async {
    final TrendingDevicesModel model = TrendingDevicesModel.fromJson(
        json.decode(Fixture('trending_devices.json')));

    when(httpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => Response(Fixture('trending_devices.json'), 200));

    final TrendingDevicesModel result =
        await remoteDataSource.getTrendingDevices();

    expect(model, result);
  });

  test('should throw server exception when response not 200', () {
    when(httpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => Response('some thing went wrong! ', 404));

    final call = remoteDataSource.getTrendingDevices;
    expect(() => call(), throwsA(TypeMatcher<ServerException>()));
  });
}
