import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/core/error/exception.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/core/network/network_info.dart';
import 'package:mobology/src/data/models/trending_device_model.dart';
import 'package:mobology/src/data/models/trending_devices_model.dart';
import 'package:mobology/src/data/repositories/repository.dart';
import 'package:mobology/src/data/resources/local_data_source.dart';
import 'package:mobology/src/data/resources/remote_data_source.dart';
import 'package:mobology/src/domain/entities/trending_device_entity.dart';
import 'package:mobology/src/domain/entities/trending_devices_entity.dart';
import 'package:mobology/src/domain/repositories/repository.dart';
import 'package:mockito/mockito.dart';

class mockRemoteDataSource extends Mock implements RemoteDataSource {}

class mockLocalDataSource extends Mock implements LocalDataSource {}

class mockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  MobologyRepository repository;
  mockRemoteDataSource remoteDataSource;
  mockLocalDataSource localDataSource;
  mockNetworkInfo networkInfo;

  TrendingDevicesModel trendingDevicesModel = TrendingDevicesModel(
    message: 'OK',
    code: 200,
    success: true,
    devices: [
      TrendingDeviceModel(
        id: 1,
        name: "samsung a40",
        company: "samsung",
        imageUrl: "www.imageUrl.com",
        deviceType: "tablet",
      ),
    ],
  );

  setUp(() {
    remoteDataSource = mockRemoteDataSource();
    localDataSource = mockLocalDataSource();
    networkInfo = mockNetworkInfo();
    repository = MobologyRepository(
      remoteDataSource: remoteDataSource,
      localDataSource: localDataSource,
      networkInfo: networkInfo,
    );
  });

  group('repository Tests', () {
    test('check device is online before requesting the data ', () {
      when(networkInfo.isConnected).thenAnswer((_) async => true);

      repository.fetchTrendingDevices();

      verify(networkInfo.isConnected);
    });

    group('device online', () {
      setUp(() {
        when(networkInfo.isConnected).thenAnswer((_) async => true);
      });

      test('should call the remote data source', () async {
        when(remoteDataSource.getTrendingDevices())
            .thenAnswer((_) async => trendingDevicesModel);

        final result = await repository.fetchTrendingDevices();

        verify(remoteDataSource.getTrendingDevices());
        expect(result, (Right(trendingDevicesModel)));
        expect(result.isRight(), true);
      });

      test('should cache data whan gotten from the remote data source',
          () async {
        when(remoteDataSource.getTrendingDevices())
            .thenAnswer((_) async => trendingDevicesModel);

        final result = await repository.fetchTrendingDevices();

        verify(remoteDataSource.getTrendingDevices());
        verify(localDataSource.cacheTrendingDevices(trendingDevicesModel));
        expect(result, (Right(trendingDevicesModel)));
        expect(result.isRight(), true);
      });

      test(
          'should return a server error when the call to remote data source fails ',
          () async {
        when(remoteDataSource.getTrendingDevices())
            .thenThrow(ServerException());

        final result = await repository.fetchTrendingDevices();

        verifyZeroInteractions(localDataSource);
        expect(result, Left(ServerFailure()));
      });
    });

    group('device is ofline', () {
      setUp(() {
        when(networkInfo.isConnected).thenAnswer((_) async => false);
      });
      test('should return a cached copy of the trending devices ', () async {
        when(localDataSource.getLastTrendingDevices())
            .thenAnswer((_) async => trendingDevicesModel);

        final result = await repository.fetchTrendingDevices();

        verifyZeroInteractions(remoteDataSource);
        verify(localDataSource.getLastTrendingDevices());
        expect(result, Right(trendingDevicesModel));
      });

      test('should return cacheFailure when a caheException is thrown ',
          () async {
        when(localDataSource.getLastTrendingDevices())
            .thenThrow(CacheException());

        final result = await repository.fetchTrendingDevices();

        verifyZeroInteractions(remoteDataSource);
        verify(localDataSource.getLastTrendingDevices());
        expect(result, Left(CacheFailure()));
      });
    });
  });
}
