import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/data/resources/local_data_source.dart';
import 'package:mockito/mockito.dart';
import 'package:sqflite/sqlite_api.dart';

class MockSqfLiteDatabase extends Mock implements Database {}

void main() {
  LocalDataSource localDataSource;
  MockSqfLiteDatabase database;
  setUp(() {
    localDataSource = LocalDataSourceI();
    database = MockSqfLiteDatabase();
  });

  //  final call = localDataSource.getLastTrendingDevices;
  // assert
  // Calling the method happens from a higher-order function passed.
  // This is needed to test if calling a method throws an exception.
  // expect(() => call(), throwsA(TypeMatcher<CacheException>()));
}
