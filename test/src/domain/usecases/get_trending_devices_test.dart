import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/core/usecase.dart';
import 'package:mobology/src/domain/entities/trending_device_entity.dart';
import 'package:mobology/src/domain/entities/trending_devices_entity.dart';
import 'package:mobology/src/domain/repositories/repository.dart';
import 'package:mobology/src/domain/usecases/get_trending_devices.dart';
import 'package:mockito/mockito.dart';

class MockRepository extends Mock implements Repository {}

void main() {
  GetTrendingDevices usecase;
  MockRepository repository;

  setUp(() {
    repository = MockRepository();
    usecase = GetTrendingDevices(repository);
  });

  final trendingDevices = TrendingDevicesEntity(
    devices: [
      TrendingDeviceEntity(
        id: 1,
        name: "samsung a40",
        company: "samsung",
        imageUrl: "www.imageUrl.com",
        deviceType: "tablet",
      ),
    ],
  );

  test('should return TrendingDevicesEntity', () async {
    when(repository.fetchTrendingDevices())
        .thenAnswer((_) async => Right(trendingDevices));

    Either<Failure, TrendingDevicesEntity> result = await usecase(NoParams());

    expect(result, Right(trendingDevices));
    verify(repository.fetchTrendingDevices());
    verifyNoMoreInteractions(repository);
  });
}
