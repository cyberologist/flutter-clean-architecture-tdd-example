import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobology/src/core/error/failure.dart';
import 'package:mobology/src/core/usecase.dart';
import 'package:mobology/src/data/models/trending_device_model.dart';
import 'package:mobology/src/data/models/trending_devices_model.dart';
import 'package:mobology/src/domain/usecases/get_trending_devices.dart';
import 'package:mobology/src/presentation/bloc/bloc.dart';
import 'package:mockito/mockito.dart';

class MockGetTrendingDevicesEvent extends Mock
    implements GetTrendingDevicesEvent {}

class MockTrendingDevicesUseCase extends Mock implements GetTrendingDevices {}

void main() {
  HomeBloc bloc;
  MockTrendingDevicesUseCase mockGetTrendingDevices;
  setUp(() {
    mockGetTrendingDevices = MockTrendingDevicesUseCase();
    bloc = HomeBloc(getDevices: mockGetTrendingDevices);
  });

  test(' initial state should be empty', () {
    expect(bloc.initialState, (Empty()));
  });
  group('GetTrendingDevices', () {
    TrendingDeviceModel deviceModel;
    TrendingDevicesModel devicesModel;
    MockTrendingDevicesUseCase mocktrendingDevicesUseCase;
    HomeBloc bloc;
    setUp(() {
      deviceModel = TrendingDeviceModel(
          id: 1, company: 'Samsung', name: 'a40', imageUrl: 'imageUrl.com');
      devicesModel = TrendingDevicesModel(
          message: 'ok', code: 200, success: true, devices: [deviceModel]);
      mocktrendingDevicesUseCase = MockTrendingDevicesUseCase();
      bloc = HomeBloc(getDevices: mocktrendingDevicesUseCase);
    });

    test('should get data from GetTrendingDevices usecase ', () async {
      when(mocktrendingDevicesUseCase.call(NoParams()))
          .thenAnswer((_) async => Right(devicesModel));

      bloc.add(GetTrendingDevicesEvent());
      await untilCalled(mocktrendingDevicesUseCase(any));

      verify(mocktrendingDevicesUseCase(NoParams()));
    });

    test('should emit empty => loading => loaded when got the data ', () {
      when(mocktrendingDevicesUseCase.call(NoParams()))
          .thenAnswer((_) async => Right(devicesModel));

      final expected = [
        Empty(),
        Loading(),
        Loaded(devicesModel.devices),
      ];
      bloc = HomeBloc(getDevices: mocktrendingDevicesUseCase);

      bloc.add(GetTrendingDevicesEvent());
      expect(bloc.asBroadcastStream(), emitsInOrder(expected));
    });

    test(
        'should emit empty => loading => Error(server failure) when data remote fetching fails ',
        () {
      when(mocktrendingDevicesUseCase.call(NoParams()))
          .thenAnswer((_) async => Left(ServerFailure()));

      final expected = [
        Empty(),
        Loading(),
        Error(SERVER_FAILURE_MESSAGE),
      ];
      bloc = HomeBloc(getDevices: mocktrendingDevicesUseCase);

      bloc.add(GetTrendingDevicesEvent());
      expect(bloc.asBroadcastStream(), emitsInOrder(expected));
    });

    test(
        'should emit empty => loading => Error(cache failure message) when local data fetching fails ',
        () {
      when(mocktrendingDevicesUseCase.call(NoParams()))
          .thenAnswer((_) async => Left(CacheFailure()));

      final expected = [
        Empty(),
        Loading(),
        Error(CACHE_FAILURE_MESSAGE),
      ];
      bloc = HomeBloc(getDevices: mocktrendingDevicesUseCase);

      bloc.add(GetTrendingDevicesEvent());
      expect(bloc.asBroadcastStream(), emitsInOrder(expected));
    });
  });
}
